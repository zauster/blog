---
title: Random walk centrality in Octave and R (with Rcpp)
author: Oliver Reiter
date: 2018-11-13
keep_md: true
categories: ["R", "Octave", "Matlab"]
tags: ["rcpp", "random graph", "input-output", "centrality"]
bibliography: [blog.bib]
link-citations: yes
excerpt: "Random walk centrality is a measure used to identify the most important, the most central nodes in a graph. Input-output tables are an example of such a graph."
---

# Random walk centrality

@blochl2011vertex `r tufte::margin_note("The paper can be downloaded from [here](https://digitalcommons.calpoly.edu/cgi/viewcontent.cgi?article=1137&context=econ_fac).")` argue that a complex system such as a national economy with its
 extensive interlinkages (which are -- at least partially --
 captured by input-output matrizes) can be characterized by the
 varying importance of industrial sectors in the production
 process. Since IO matrizes a) are usually fully connected, b)
 have a clear direction (i.e., the flow of goods from one sector
 to another) and c) contain large self-loops, @blochl2011vertex
 propose to use a random walk centrality measure.

@blochl2011vertex published [Matlab/Octave
code](https://www.helmholtz-muenchen.de/icb/software/input-output-networks/index.html)
that can be used to calculate the two measures they describe in
the paper. When I wanted the re-program their functions in R, I
came across two issues with their code:

1. They state in their paper that they use the [Sherman-Morrison
   formula](https://en.wikipedia.org/wiki/Sherman%E2%80%93Morrison_formula)
   which allows the efficient updating of an inverse. In the
   published code however, they do calculate the inverse (with
   `inv`) of the matrix in *every iteration* step (while
   additionally computing the vectors that would have been
   necessary for the Sherman-Morrison formula).
2. The actual implementation of the Sherman-Morrison formula is
   faulty: Since they update one column and one row in every
   iteration step, they update the diagonal element $a_{ii}$
   *twice*, thus producing wrong updates to the inverse matrix
   and wrong results in the end.

The following Matlab/Octave code is adapted from the code that is
available
[homepage](https://www.helmholtz-muenchen.de/icb/software/input-output-networks/index.html)
of the authors. I corrected the mistakes described above and
indicated this in the code as well:

```Matlab
function cen = rwcentrality(A)
  n = size(A, 1);
  H = zeros(n); % preallocate MFHT matrix
  IM = eye(n) - inv(diag(sum(A'))) * A; % transition matrix
  IMinv = inv(IM(2:end, 2:end));
  for i = 1:n % iterate over all nodes sets the whole column i,
              % except row i.
    H([1:(i - 1) (i + 1):n], i) = sum(IMinv, 2);
    if i < n % compute next inverse by Sherman Morrison
      u = IM([1:i (i + 2):n], i) - IM([1:(i - 1) (i + 1):n], i + 1);

      ## Issue 2: update the element ii only by half
      u(i) = u(i) / 2;
      IMinv = IMinv - ((IMinv*u)*IMinv(i, :)) ./(1 + IMinv(i, :)*u);
      v = IM(i, [1:i (i + 2):n]) - IM(i + 1, [1:(i - 1) (i + 1):n]);

      ## Issue 2: update the element ii with the other half
      v(i) = v(i) / 2;
      IMinv = IMinv - (IMinv(:, i)*(v*IMinv)) ./(1 + v*IMinv(:, i));

      ## Issue 1: no need to calculate the whole inverse again,
      ## if we have done a correct update. So the next line can be
      ## commented and even deleted.
      ## IMinv = inv(IM([1:i (i + 2):n], [1:i (i + 2):n]));

      if any(~isfinite(IMinv)) % Sherman Morrison didn't work
        IMinv = inv(IM([1:i (i + 2):n], [1:i (i + 2):n]));
      end
    end
  end
  cen = n ./ sum(H);
end
```

# R implementation

Since I use RcppArmadillo for the implementation (and since
Armadillo's syntax was explicitly designed after Matlab's
syntax), the translation was not very difficult, check the code
out
[here](https://gitlab.com/zauster/riot/blob/master/src/rwCentrality.cpp).


# Benchmarking comparison

Inital comparison of the runtimes of the octave implementation by
the authors (corrected for the mistakes) and my R + RcppArmadillo
implementation.

First I run an example script in `octave`. After a test with a
small test matrix I test the function on the Austrian
input-output matrix for the year 2000 and take the time.

```Matlab
octave> A = [1,2,2,4; 2,5,6,1; 3,7,1,4; 8,5,4,1]
A =

   1   2   2   4
   2   5   6   1
   3   7   1   4
   8   5   4   1

octave> rwcentrality(A)
ans =

   0.30037   0.41478   0.40419   0.29603

octave>
octave> ## test with AUT 2000
octave> A = load("IOdownload.mat");
octave> Amat = A.io.aut2000;
octave>
octave> ## delete the rows/columns with only 0
octave> index = [1:9, 11:13, 15:22, 25, 26, 29:47];
octave> Amat = Amat(index, index);
octave> whos Amat
Variables in the current scope:

   Attr Name        Size                     Bytes  Class
   ==== ====        ====                     =====  =====
        Amat       41x41                     13448  double

Total is 1681 elements using 13448 bytes

octave>
octave> tic()
octave> x1 = rwcentrality(Amat);
octave> toc()
Elapsed time is 0.0152152 seconds.
```

Then I do the same in `R`. First loading some needed libraries,
then checking whether the results for the small test matrix are
indeed the same as in the Matlab/Octave code (they are!). 

Afterwards I use the `readMat` function from the
[R.matlab](https://cran.r-project.org/web/packages/R.matlab/)
package and calculate the random walk centralities for Austria again, again checking the elapsed time.

The RcppArmadillo function is about 6.8 times faster than the
Octave one, which is actually less than expected. It would be
interesting to test it on a bigger matrizes too, e.g. a full WIOD
table for one year (one such IO matrizes has dimensions of 2700 x
2700).


```r
> library(riot)
> library(microbenchmark)
> library(R.matlab)
> options(digits = 5)
>
> A = matrix(data = c(1,2,2,4,2,5,6,1,3,7,1,4,8,5,4,1), ncol = 4, byrow = TRUE)
> rwCentrality(A)
[,1]    [,2]    [,3]    [,4]
[1,] 0.30037 0.41478 0.40419 0.29603
>
> A.list <- readMat("IOdownload.mat")
> Amat <- A.list[["io"]][[3]]
>
> index <- c(1:9, 11:13, 15:22, 25, 26, 29:47)
> Amat <- Amat[index, index]
> dim(Amat)
[1] 41 41
>
> microbenchmark(rwCentrality(Amat), unit = "s")
Unit: seconds
expr       min        lq      mean    median
rwCentrality(Amat) 0.0022816 0.0023151 0.0024487 0.0023356
uq      max neval
0.0024101 0.004195   100
```

# References
