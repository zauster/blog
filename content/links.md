---
title: Links and other resources
---

A list of stuff I find interesting and/or important to link:

- [wiiw](www.wiiw.ac.at): my current employer
- [Gitlab repo](https://gitlab.com/zauster): my Gitlab repo, more active than Github
- [Github repo](https://github.com/zauster): my Github repo, not all repos are updated regularly here
- R packages I have authored:
  - [npExact](https://cran.r-project.org/web/packages/npExact/index.html)
- R packages I have contributed to:
  - [eurostat](https://github.com/rOpenGov/eurostat/pull/41)
  - [texreg](https://github.com/leifeld/texreg/pull/102)
  - [decompr](https://github.com/bquast/decompr/pull/10)
  - [wiod](https://github.com/bquast/wiod/pull/3)
